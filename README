General Information
===================

libcdaudio is a library designed to provide functions to control
operation of a CD-ROM when playing audio CDs.  It also contains
functions for CDDB and CD Index lookup.

Platforms
=========

libcdaudio will compile under the following platforms.  Listed
underneath them are any functions that will not work on that platform.

Linux 1.2: CD-ROM changer functions will not work at all.

Linux 2.0: You may select discs using the cd_changer_select_disc()
function, but the cd_changer_slots() function will not work at all,
and consequently, the cd_changer_stat() function will not work as
well.

Linux 2.1 and later: All functions should work correctly.

OpenBSD 2.3: The tray open/close functions may not work correctly.
CD-ROM changer functions will not work at all.

FreeBSD 3.2: A kernel bug prevents you from ejecting the tray when
there is no disc present.  CD-ROM changer functions will not work at
all.

NetBSD: Might work (never tested).  Please tell me if you can test
libcdaudio on NetBSD.

Solaris 2.5, 2.6: The tray open/close functions may not work
correctly.  CD-ROM changer functions will not work at all.
cd_get_volume() will always return 0 for the volumes.

Irix: As far as I know all function work within Irix.

BeOS: A user contributed the file beos_cdaudio.c; it seems to work,
except for CD changers.

OSF/1: libcdaudio has limited and untested support for OSF/1.
Although it may be able to list the tracks and read the CDDB/CD Index
information, playing will probably fail and stopping will not work at
all.  If you are willing to test this on OSF/1, please e-mail me at
noon@users.sourceforge.net.

XBox: Most functions should work correctly.  The XBox seems to lack
the direct CDdrive->soundcard cable that is necessary to hear
anything, though.

Installation
============

If you have received libcdaudio as part of another package, read the
documentation in the top level source directory first to see if you
need to compile libcdaudio separately.

For help with installation, please see the file INSTALL.

Thanks
======

Thanks go to:

Antony Arcieri, original author.
Mike Oliphant for parts of the HTTP code.
Ralph Wallace for ports to FreeBSD.
Ryan Banks for incomplete ports to Digital UNIX.
Justus Pendelton for ports to Solaris.
Matt Kraai for additional Solaris help.
David Rose for ports to Irix.
Ryan Werber for a box to test the CD-ROM changer code on.
Quinton Dolan for internal optimisations.
Asheesh Laroia for making libcdaudio work on the XBox.

If you aren't in this list, and you think you should be, just tell me.

Bugs
====

All bugs reports should be sent to noon@users.sourceforge.net.

If you do experience any problems compiling, please send me:

* The version of libcdaudio
* Your platform type and release
* the arguments and output of the configure script
* A complete list of errors that occurred during the compile
(e.g. make >& errors) and the configuration.
* The CD-ROM header file for your platform so I can modify libcdaudio
accordingly
* Any other information you believe is relevant

If you experience problems during the operation of programs compiled
with this library, be sure to send:

* The version of libcdaudio
* The name and version of the program with which the error occurred
* Your platform type and release
* Any other information you believe is relevant

Patches
=======

Send all patches to: noon@users.sourceforge.net

Be sure to include your name so I can give credit where it is due.  If
the patch fixes a bug, it would be nice if you could send the patch
along with the bug report.

Licence
=======

libcdaudio is distributed under the GNU Library General Public
License, included in this package under the top level source directory
in the file COPYING.

Projects interested in libcdaudio
=================================

This list is useful for me to learn what people want about libcdaudio,
how it is used by them, how libcdaudio should evolve to meet
everyone's needs.

If you know of a program that should be added in this list, feel free
to tell me, please.

bebocd
cajun (http://www.dvhart.com/projects/cajun/)
cddb-slave, cddb-slave2, gnome-cd, gnome-media (http://mail.gnome.org/archives/gnome-multimedia/2002-September/msg00030.html)
cdplayer.app (http://tucows-linux.up.pt/preview/9098.html)
demcd
disc-cover (http://homepages.cwi.nl/~jvhemert/disc-cover.html)
dmc, dmcd
freeamp/zinf borrowed code from libcdaudio
www.freedb.org (http://www.dtype.org/pipermail/fdb-apps/2002-August.txt)
fxcd
globecom jukebox (http://sourceforge.net/projects/gjukebox/)
gnome-media (gnome-cd)
grip (uses a fork from libcdaudio, it seems)
low-grimoire (at BerliOS)
macos x (http://macosx.forked.net/bbarchive/forum5/000116.html)
massrip (http://kfa.cx/)
matrix-devices devcdplayer (http://futurelab.aec.at/matrix/02_docu/devdoc/devdoc.html)
mp3make2
musicbrainz
mycd (http://un1c0.freewebpage.org/)
mythtv/mythmusic (http://www.mythtv.org/docs/mythtv-HOWTO-15.html)
open music machine (http://pficheux.free.fr/omm/)
http://ozcdplayer.sourceforge.net/
paloma
perl Audio::CD
pycdaudio
rbcd (Ruby)
record manager (http://www.theossoft.net/index.html)
rhythmbox (http://www.rhythmbox.org/)
ripper
sdl
simplecd (http://nofx.cse.unsw.edu.au/code/simplecd.pml)
sumi (xtunes.sf.net)
a tcl/tk package (http://tcltk.free.fr/index.php3?idxcat=3)
uraten (http://www2.pos.to/~rero2/works/uraten/uraten_index.html)
the witty cd player (http://www.softonic.com/ie/17060)
wmsvencd
xmms (currently uses its own internal library)
yaret (http://www.nongnu.org/yaret/)
